# telegram_basic_bot

## Intro
This package contains the basic functionality telegram bot. You can extend it with your own methods and commands.
The bot comes as a class with all necessary built-ins to simply run it out of the box by executing the `run_bot` method. 
The functionality is based on the aiogram library.

## Configuration
The bot requires configuration and vocabulary files. They are yaml files loaded at start up. 
 
Bot language and vocabulary file must be indicated to properly use the built-in vocabulary. 
It may change in the next versions.

## Usage

```python
from telegram_basic_bot import BasicBot
from aiogram.types import Message

class MyBot(BasicBot):
    def __init__(self, conf_file: str):
        super().__init__(conf_file)
    # override start method
    async def start(self, msg: Message):
        pass # change start greeting
    
    # Override text message processing
    async def text_message_handler(self, msg: Message):
        pass # do what you want here
    # Add your own command
    async def my_super_duper_command(self, msg: Message):
        pass # write the command logic here

```

Custom method name should be the same as the intended command name and should not start with an underscore ('_'). 
This is a limitation to facilitate automated action menu building. 
The bot has a built-in user database. You can check against it in your commands to validate the user. 
However, it is recommended to use an external database for more complex checks.



