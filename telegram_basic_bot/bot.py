from logging import info, debug, error

from aiogram import Bot, Dispatcher, executor
from aiogram.types import Message # , InlineKeyboardMarkup, InlineKeyboardButton, CallbackQuery

from .configuration import BasicBotConfig
from .vocabulary import BotVocabulary
from .bot_db import BotDatabase, Error



class BasicBot:
    """
    This is the main telegram bot class. Extend it to make a custom bot
    """
    __slots__ = ('bot_config', 'bot_db', 'bot', 'dp', 'menu_items', 'menu', 'vocabulary')

    # The following method names are reserved for the bot use. Use them to exclude from a bot menu.
    _PROTECTED_NAMES = ('run_bot', 'start', 'text_message_handler')

    def __init__(self, conf_file: str):
        """
        Initializes a BasicBot class instance

        :param conf_file: configuration file path
        """
        # This is bot configuration.
        try:
            self.bot_config = BasicBotConfig(conf_file)
        except FileNotFoundError:
            print('Could not find config file.')
            exit(5)

        # This is a user database.
        try:
            self.bot_db = BotDatabase(self.bot_config.database_filename)
        except Error as e:
            msg = f'Failed to initialize the database: {e}'
            error(msg)
            print(msg)
            exit(7)

        # The aiogam bot and the bot dispatcher.
        self.bot = Bot(token=self.bot_config.token)
        self.dp = Dispatcher(self.bot)
        self.dp.register_message_handler(self.start,
                                         commands=['start'])

        # This is the bot vocabulary. The chosen language should be present in the vocabulary file.
        self.vocabulary = BotVocabulary(self.bot_config.vocabulary_filename, self.bot_config.language)
        info('start bot')

    async def start(self, msg: Message):
        """
        Default handler for the start command

        :param msg: telegram message to process
        """
        try:
            assert self.bot_db.check_user(msg.from_user.id)

        except AssertionError:
            info(f'Failed to login. {msg.from_user.id} {msg.from_user.full_name}')
            await msg.answer(self.vocabulary.user_not_registered)
        else:
            await msg.answer(f'{self.vocabulary.greeting}, {msg.from_user.first_name}')

    async def text_message_handler(self, msg: Message):
        """
        Default handler for text messages

        :param msg: telegram message to process
        """
        debug(f"Message from {msg.from_user.full_name}. It's a bot: {msg.from_user.is_bot}")
        if msg.text.lower() == self.vocabulary.greeting.lower():
            await msg.answer(self.vocabulary.greeting)
        else:
            await msg.answer(self.vocabulary.default_response)

    def run_bot(self):
        """
        Starts bot execution

        """
        executor.start_polling(self.dp)
