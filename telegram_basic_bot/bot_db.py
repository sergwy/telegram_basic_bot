from sqlite3 import connect as conn_db, Error, OperationalError, Cursor
from logging import info, error, debug
from dataclasses import dataclass


class SqliteWrapper:
    """This is a wrapper for the SQLite to make it easier to use"""

    def __init__(self, config_filepath: str):
        self.config_filepath = config_filepath
        self.db = conn_db(self.config_filepath)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.db.close()

    def execute(self, sql_str: str, arg: dict = None) -> Cursor:
        with self.db:
            cur = self.db.cursor()
            return cur.execute(sql_str, arg) if arg is not None else cur.execute(sql_str)


@dataclass
class BotUser:
    """User of the bot authorized to use it"""
    __slots__ = ('user_name', 'is_authorized')
    user_name: str
    is_authorized: bool

    @classmethod
    def create_from_database_row(cls, row: list | tuple):
        """
        Creates a class instance from the database row data

        :param row: bot database user table row data
        :return: BotUser
        """
        return cls(row[0], row[1])


class BotDatabase:
    """
    This is the bot database default representation class. It uses sqlite as the backend.

    """

    __slots__ = ('data_file_path',)

    def __init__(self, data_file_path: str):
        self.data_file_path = data_file_path

        with SqliteWrapper(self.data_file_path) as bot_db:
            try:
                bot_db.execute('select count(*) from bot_users')
            except OperationalError:
                # The database is empty create it
                bot_db.execute(
                        """
    create table  IF NOT EXISTS bot_users 
    (user_id INTEGER PRIMARY KEY AUTOINCREMENT, 
    user_name TEXT unique, 
    is_authorized boolean default false);
    """
                    )

    def get_bot_user(self, in_user_name: str) -> BotUser:
        """
        Retrieves a user from the database

        :param in_user_name: username to get
        :return: BotUser
        """
        with SqliteWrapper(self.data_file_path) as bot_db:

            try:
                debug(f'Getting user {in_user_name}.')
                user_row = bot_db.execute(
                    f'select user_name, is_authorized from bot_users '
                    f'where user_name = :user_name', {'user_name': in_user_name}).fetchone()

            except (ValueError, TypeError) as err:
                debug(f'Getting user: {err}')
                user = BotUser('', False)
            else:
                user = BotUser.create_from_database_row(user_row) if user_row is not None else BotUser('', False)
                debug(f'Returning user {user.user_name}; authorized to use {user.is_authorized}')

            return user

    def add_bot_user(self, bot_user: BotUser):
        """
        Adds a new bot user

        :param bot_user: bot user to add
        """
        with SqliteWrapper(self.data_file_path) as bot_db:
            try:
                bot_db.execute(
                    f'insert into bot_users (user_name, is_authorized) values (:user_name, :is_authorized)',
                   {'user_name': bot_user.user_name, 'is_authorized': bot_user.is_authorized})

                info(f'User {bot_user.user_name} has been added.')
            except Error as e:
                error(f'Failed to add a user! {e}')

    def check_user(self, in_user_name: str) -> bool:
        """
        Checks if the user is valid and authorized

        :param in_user_name: username to check
        :return: bool
        """
        return self.get_bot_user(in_user_name).is_authorized
