from logging import debug, error, warning
from yaml import load, SafeLoader


class BotVocabulary:
    def __init__(self, vocabulary_file: str, language: str):
        self.greeting = None
        self.help = None
        self.default_response = None
        self.wrong_request = None
        self.user_not_registered = None

        try:
            with open(vocabulary_file, 'rt') as vocabulary_data:
                loaded_data = load(vocabulary_data.read(), Loader=SafeLoader)

            assert language.lower() in loaded_data['supported_languages']

        except FileNotFoundError:
            error('Could not find the vocabulary file. Empty vocabulary initialized.')
        except AssertionError:
            warning('The configured language is not supported by the vocabulary. Empty vocabulary initialized.')
        else:
            for key, value in loaded_data.items():
                try:
                    setattr(self, key, value[language.lower()])
                except KeyError:
                    warning(f'Skip vocabulary data in {key}')
                except TypeError:
                    debug('Skip supported_languages list.')

        debug(self.__dict__)
