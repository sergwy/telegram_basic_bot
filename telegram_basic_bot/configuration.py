import logging
from logging import NOTSET, info, debug
from yaml import load, SafeLoader


class BasicBotConfig:
    """
    Class representing the bot configuration
    """
    def __init__(self, file_path: str):
        """
        Initializes the BasicBotConfig class instance
        :param file_path: configuration file path
        """
        self.logging_level: str = 'NOTSET'
        self.logging_filename: str | None = None
        self.language = 'english'
        self.token: str = ''
        self.vocabulary_filename: str | None = None
        self.database_filename: str | None = None
        with open(file_path, 'rt') as config_file:
            loaded_data = load(config_file.read(), Loader=SafeLoader)

            # Setting attributes from config
            for key, value in loaded_data.items():

                if not isinstance(value, dict):
                    setattr(self, key, value)
                else:
                    for k, v in value.items():
                        setattr(self, f'{key}_{k}', v)

            # Configuring logging
            try:
                self.log_level = getattr(logging, self.logging_level.upper())
                logging.basicConfig(filename=self.logging_filename,
                                    format='%(asctime)s.%(msecs)03d %(name)s %(levelname)-8s %(message)s',
                                    datefmt='%Y-%m-%d %H:%M:%S',
                                    level=self.log_level)
                info('Loaded configuration file.')
                debug(self.__dict__)
            except AttributeError:
                print('Logging configuration was not loaded. Defaulting to NOTSET')
                self.log_level = NOTSET
